import numpy
import os
import math 
import sys
import time
from time import sleep

class graphics:
    def __init__(self):
        pass

    def create_window(self, x, y):
        pass

    def draw_squares(self, x, y):
        pass

class mechanics:
    def __init__(self):
        pass
    
    def create_2d_matrix(y, x):
        matrix = []
        for i in range(y):
            matrix.append([])
            for _ in range(x):
                matrix[i].append(0)
        return matrix



    def define_obstacles(self):
        pass

class logic:
    def __init__(self):
        pass
    
    def clear():
        os.system('cls')

    def calculate_distance(goal_x, goal_y, ai_x, ai_y):
        distance_from_goal = math.sqrt((goal_y - ai_y)**2 + (goal_x - ai_x)**2)
        return distance_from_goal

    def find_shortest_path(FIELD, y, x, end):
        FIELD[y][x] = 2
        dists = []
        if FIELD[y][x] == 3:
            return
        
        for i in range(y-1, y+2):
            for n in range(x-1, x+2):
                logic.clear()
                if i >= 0 and n >= 0 and i < len(FIELD) and n < len(FIELD[0]):  
                    if FIELD[i][n] == 0:
                        # FIELD[i][n] = 2
                        dist_now = logic.calculate_distance(*end, x, y)
                        dist_future = logic.calculate_distance(*end, n, i)
                        if dist_now > dist_future:
                            dists.append((n, i, dist_future))
                
                print(numpy.matrix(FIELD))
                # sleep(0.5)
        
        # decide next move
        #print(dists)
        onli_dist = []
        for data in dists:
            _, _, distance = data
            onli_dist.append(distance)
        
        try:
            shortest_dist = min(onli_dist)
        except ValueError:
            print('no path found ')
        
        else:
            for data in dists:
                if shortest_dist in data:
                    new_x, new_y, diststance = data
                    end_x, end_y = end
                    FIELD[new_y][new_x] = 2
                
                    if new_x == end_x and new_y == end_y:
                        logic.clear()
                        FIELD[new_y][new_x] = 3
                        print(numpy.matrix(FIELD))
                        return
                    
                    logic.find_shortest_path(FIELD, new_y, new_x, end)
        




if __name__ == '__main__':
    time_start = time.time()
    field = mechanics.create_2d_matrix(20, 20)
    logic.find_shortest_path(field, 0, 0, (len(field)-1, len(field)-1))
    time_end = time.time()-time_start
    print(f'''\nProgram finished after {time_end:.3} seconds''')