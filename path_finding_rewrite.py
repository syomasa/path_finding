import numpy
import os
import math 
import sys
import time
import random
from time import sleep

class Matrix:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.matrix = []

    def create_2d_matrix(self):
        for i in range(self.y):
            self.matrix.append([])
            for n in range(self.x):
                self.matrix[i].append(0)

    def add_walls(self, ammount_of_walls):
        free_coords = []
        for y, a in enumerate(self.matrix):
            for x, b in enumerate(self.matrix[0]):
                if self.matrix[y][x] == 0:
                    free_coords.append((x, y))
   
        for i in range(ammount_of_walls):
            rnd_square = random.choice(free_coords)
            self.matrix[rnd_square[0]][rnd_square[1]] = 1
            free_coords.remove(rnd_square)

            
class Pathing(Matrix):
    @staticmethod
    def clear():
        os.system('cls')
    
    @staticmethod
    def calculate_distance(startx, starty, endx, endy):
        distance = math.sqrt((endx-startx)**2 + (endy-starty)**2)
        return distance

    def find_shortest_path(self, y, x, end):
        self.matrix[y][x] = 2
        dists = []
        for i in range(y-1, y+2):
            for n in range(x-1, x+2):
                Pathing.clear()
                if i >= 0 and n >= 0 and i < len(self.matrix) and n < len(self.matrix[0]):
                    if self.matrix[i][n] == 0:
                        dist_now = Pathing.calculate_distance(x, y, *end)
                        dist_future = Pathing.calculate_distance(n, i, *end)
                        if dist_now > dist_future:
                            dists.append((n, i, dist_future))
                
                print(numpy.matrix(self.matrix))

        onli_dists = []
        for data in dists:
            _, _, dist = data
            onli_dists.append(dist)

        try:
            shortest_dist = min(onli_dists)
            onli_dists.remove(shortest_dist)
        except ValueError:
            print('no path found')
        else:
            for data in dists:
                if shortest_dist in data:
                    new_x, new_y, _ = data
                    end_x, end_y = end

                    if new_x == end_x and new_y == end_y:
                        Pathing.clear()
                        self.matrix[new_y][new_x] = 3
                        print(numpy.matrix(self.matrix))
                        input()
                        return
                    
                    else:
                        self.matrix[new_y][new_x] = 2
                        self.find_shortest_path(new_y, new_x, end)
                        self.matrix[new_y][new_x] = 0


    
    



if __name__ == '__main__':
    
    pt = Pathing(10, 10)
    pt.create_2d_matrix()
    pt.add_walls(30)
    pt.find_shortest_path(0, 0, (pt.x-1, pt.y-1))
